# coding=utf-8

import sys
import re
import time

formatFull = r'#define BUILD_NUMBER[\s]+0x[\d]+'
formatVersion = r'#define BUILD_NUMBER[\s]+0x([\d]+)'

if(len(sys.argv) == 2):
    fileName = sys.argv[1]
    file = open(fileName)
    content = file.read()
    file.close()
    aimFulls = re.findall(formatFull, content)
    aimVersions = re.findall(formatVersion, content)
    print(aimFulls)
    print(aimVersions)
    if(len(aimFulls) == 1 or len(aimVersions) == 1):
        oldDefine = aimFulls[0]
        oldVersion = aimVersions[0]
        print("Old Version is:\t" + oldVersion)
        newVersion = time.strftime("%m%d%H%M", time.localtime())
        print(newVersion)
        newDefine = oldDefine.replace(oldVersion, newVersion)
        print("New define is:\t" + newDefine)
        content = content.replace(oldDefine, newDefine)
        file = open(fileName, 'w')
        file.write(content)
        file.close()
     
    
